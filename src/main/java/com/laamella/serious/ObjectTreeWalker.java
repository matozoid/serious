package com.laamella.serious;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static com.laamella.serious.Toolbag.getAllFieldsOnClass;
import static com.laamella.serious.Toolbag.getFieldValue;

public class ObjectTreeWalker {
    public String print(Object rootObject, Instructions instructions) {
        return print(rootObject, instructions, new ObjectRegistry()).get();
    }

    private Supplier<String> print(Object object, Instructions instructions, ObjectRegistry objectRegistry) {
        if (object == null) {
            return instructions::nullText;
        }
        if (!instructions.visitObject(object)) {
            return () -> "";
        }
        Optional<String> toStringed = instructions.toStringThisObject(object);
        if (toStringed.isPresent()) {
            return toStringed::get;
        }

        final ObjectRegistry.ObjectMeta objectMeta = objectRegistry.registerObject(object);

        if (objectMeta.occursMoreThanOnce) {
            return printBackReference(object, instructions, objectMeta);
        }
        if (object instanceof Collection) {
            return printCollection((Collection<?>) object, instructions, objectRegistry);
        }
        if (object instanceof Map) {
            return printMap((Map<?, ?>) object, instructions, objectRegistry);
        }
        return printFields(object, instructions, objectRegistry);

    }

    private Supplier<String> printBackReference(Object object, Instructions instructions, ObjectRegistry.ObjectMeta objectMeta) {
            return () -> instructions.backReferenceText(object, objectMeta.getId());
    }

    private Supplier<String> printCollection(Collection<?> object, Instructions instructions, ObjectRegistry objectRegistry) {
        // TODO sort unordered collections
        final StringSuppliers suppliers = new StringSuppliers(() -> "[", () -> ",", () -> "]");
        for (Object o : object) {
            suppliers.add(print(o, instructions, objectRegistry));
        }
        return suppliers;
    }

    private Supplier<String> printMap(Map<?, ?> object, Instructions instructions, ObjectRegistry objectRegistry) {
        // TODO sort unordered collections
        final StringSuppliers suppliers = new StringSuppliers(() -> "{", () -> ",", () -> "}");
        for (Map.Entry<?, ?> o : object.entrySet()) {
            Supplier<String> key = print(o.getKey(), instructions, objectRegistry);
            Supplier<String> value = print(o.getValue(), instructions, objectRegistry);
            suppliers.add(() -> key.get() + instructions.mapEntryArrowText() + value.get());
        }
        return suppliers;
    }

    private Supplier<String> printFields(Object object, Instructions instructions, ObjectRegistry objectRegistry) {
        final StringSuppliers suppliers = new StringSuppliers(() -> {
            ObjectRegistry.ObjectMeta meta = objectRegistry.getMeta(object);
            if (meta.occursMoreThanOnce) {
                return instructions.objectWithIdText(object, meta.getId()) + "(";
            } else {
                return object.getClass().getSimpleName() + "(";
            }
        }, () -> ",", () -> ")");
        List<Field> fields = getAllFieldsOnClass(object.getClass());
        fields.stream()
                .filter(f -> instructions.visitField(object, f))
                .forEach(f -> {
                    Supplier<String> value = print(getFieldValue(object, f), instructions, objectRegistry);
                    suppliers.add(() -> f.getName() + "=" + value.get());
                });
        return suppliers;
    }

}