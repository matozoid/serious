package com.laamella.serious;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class StringSuppliers implements Supplier<String> {
	private final List<Supplier<String>> suppliers = new ArrayList<>();

	private final Supplier<String> prefix;
	private final Supplier<String> infix;
	private final Supplier<String> postfix;

	public StringSuppliers(Supplier<String> prefix, Supplier<String> infix, Supplier<String> postfix) {
		this.prefix = prefix;
		this.infix = infix;
		this.postfix = postfix;
	}

	public void add(Supplier<String> supplier) {
		suppliers.add(supplier);
	}

	@Override
	public String get() {
		StringBuilder buf = new StringBuilder(prefix.get());
		boolean sep = false;
		for (Supplier<String> sup : suppliers) {
			if (sep) {
				buf.append(infix.get());
			}
			sep = true;
			buf.append(sup.get());
		}
		buf.append(postfix.get());
		return buf.toString();
	}
}
