package com.laamella.serious;

import java.lang.reflect.Field;
import java.util.Optional;

public interface Instructions {
    Optional<String> toStringThisObject(Object object);

    boolean visitField(Object object, Field f);

    boolean visitObject(Object object);

    String nullText();

    String backReferenceText(Object object, int id);

    String idText(int id);

    String mapEntryArrowText();

    String objectWithIdText(Object object, int id);

    String objectText(Object object);

    // TODO
    // int maximumCollectionItemsToShow();

    // TODO
    // int maximumStringLength();
}
