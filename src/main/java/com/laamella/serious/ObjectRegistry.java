package com.laamella.serious;

import java.util.HashMap;
import java.util.Map;

public class ObjectRegistry {
	private int ID_GENERATOR = 0;

	public class ObjectMeta {
		private int id = -1;
		public boolean occursMoreThanOnce = false;

		public int getId() {
			if (id == -1) {
				id = ID_GENERATOR++;
			}
			return id;
		}
	}

	private final Map<Object, ObjectMeta> seenObjects = new HashMap<>();

	public ObjectMeta registerObject(Object o) {
		if (o == null) {
			return null;
		}
		ObjectMeta objectMeta = seenObjects.get(o);
		if (objectMeta != null) {
			objectMeta.occursMoreThanOnce = true;
			return objectMeta;
		}
		objectMeta = new ObjectMeta();
		seenObjects.put(o, objectMeta);
		return objectMeta;
	}

	public ObjectMeta getMeta(Object o) {
		ObjectMeta objectMeta = seenObjects.get(o);
		if (objectMeta == null) {
			throw new IllegalArgumentException();
		}
		return objectMeta;
	}
}

