package com.laamella.serious;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.laamella.serious.Toolbag.isPrimitive;
import static com.laamella.serious.Toolbag.toSubScriptString;

public class DefaultInstructions implements Instructions {
    private final BiFunction<Object, Field, Boolean> visitField;
    private final Function<Object, Boolean> visitObject;
    private final List<Function<Object, Optional<String>>> toStringers = new ArrayList<>();

    public DefaultInstructions() {
        this((o, f) -> (!Modifier.isStatic(f.getModifiers()))
                        && o.getClass() != Object.class
                , o -> true);
        addToStringer(DefaultInstructions::primitiveToString);
    }

    public DefaultInstructions(BiFunction<Object, Field, Boolean> visitField, Function<Object, Boolean> visitObject) {
        this.visitField = visitField;
        this.visitObject = visitObject;
    }

    @Override
    public Optional<String> toStringThisObject(Object object) {
        for (Function<Object, Optional<String>> toStringer : toStringers) {
            Optional<String> apply = toStringer.apply(object);
            if (apply.isPresent()) {
                return apply;
            }
        }
        return Optional.empty();
    }

    public static Optional<String> primitiveToString(Object o) {
        if (isPrimitive(o.getClass())) {
            return Optional.of(o.toString());
        }
        return Optional.empty();
    }

    public DefaultInstructions addToStringer(Function<Object, Optional<String>> newToStringFunction) {
        toStringers.add(newToStringFunction);
        return this;
    }

    @Override
    public boolean visitField(Object object, Field f) {
        return visitField.apply(object, f);
    }

    @Override
    public boolean visitObject(Object object) {
        return visitObject.apply(object);
    }

    @Override
    public String nullText() {
        return "null";
    }

    @Override
    public String backReferenceText(Object object, int id) {
        if (object instanceof Collection || object instanceof Map) {
            return "↩" + idText(id);
        }
        return "↩" + objectText(object) + idText(id);
    }

    @Override
    public String idText(int id) {
        return toSubScriptString(id);
    }

    @Override
    public String mapEntryArrowText() {
        return "→";
    }

    @Override
    public String objectWithIdText(Object object, int id) {
        return objectText(object) + idText(id);
    }

    @Override
    public String objectText(Object object) {
        return object.getClass().getSimpleName();
    }
}
