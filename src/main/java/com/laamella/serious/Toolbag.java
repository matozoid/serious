package com.laamella.serious;

import java.lang.reflect.Field;
import java.util.*;
import java.util.function.BiConsumer;

import static java.util.stream.Collectors.toList;

/**
 * The instrospection tools that the ObjectTreeWalker needs.
 */
public class Toolbag {
	public static Object getFieldValue(Object declaringObject, Field field) {
		try {
			field.setAccessible(true);
			return field.get(declaringObject);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Introspects the class and finds all its fields, including the fields on its superclasses, private fields, static fields, etc., and orders them by name.
	 */
	public static List<Field> getAllFieldsOnClass(Class<?> c) {
		List<Field> fields = new ArrayList<>();
		getAllFieldsOnClass(c, fields);
		Collections.sort(fields, (o1, o2) -> o1.getName().compareTo(o2.getName()));
		return fields;
	}

	private static void getAllFieldsOnClass(Class<?> startClass, List<Field> fields) {
		fields.addAll(Arrays.asList(startClass.getDeclaredFields()));

		final Class<?> parentClass = startClass.getSuperclass();
		if (parentClass != null) {
			getAllFieldsOnClass(parentClass, fields);
		}
	}

	/**
	 * @return is the supplied class one of Java's primitive types?
	 */
	public static boolean isPrimitive(Class<?> c) {
		return c == String.class ||
				c == Integer.class ||
				c == Long.class ||
				c == Character.class ||
				c == Double.class ||
				c == Boolean.class ||
				c == Byte.class ||
				c == Short.class ||
				c == Float.class;
	}

	public final static char[] superScriptChars = {'⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹'};
	public final static char[] subScriptChars = {'₀', '₁', '₂', '₃', '₄', '₅', '₆', '₇', '₈', '₉'};

	public static String toSuperScriptString(int number) {
		return toSpecialNumberString(number, superScriptChars);
	}

	public static String toSubScriptString(int number) {
		return toSpecialNumberString(number, subScriptChars);
	}

	public static String toSpecialNumberString(int number, char[] digits) {
		if (number < 0) {
			throw new IllegalArgumentException("Can't convert negative numbers");
		}
		if (number == 0) {
			return "" + digits[0];
		}
		StringBuilder stringBuilder = new StringBuilder();
		while (number > 0) {
			stringBuilder.insert(0, digits[number % 10]);
			number /= 10;
		}
		return stringBuilder.toString();
	}
}
