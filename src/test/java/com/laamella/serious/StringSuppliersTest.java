package com.laamella.serious;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringSuppliersTest {
	@Test
	public void test1() {
		StringSuppliers suppliers = new StringSuppliers(() -> "<", () -> "|", () -> ">");
		suppliers.add(() -> "A");
		suppliers.add(() -> "B");
		assertEquals("<A|B>", suppliers.get());
	}
}