package com.laamella.serious;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToolbagTest {
	@Test
	public void superScript() {
		String s = Toolbag.toSuperScriptString(1234567890);
		assertEquals("¹²³⁴⁵⁶⁷⁸⁹⁰", s);
	}

	@Test
	public void zero() {
		String s = Toolbag.toSuperScriptString(0);
		assertEquals("⁰", s);
	}

	@Test
	public void subScript() {
		String s = Toolbag.toSubScriptString(1234567890);
		assertEquals("₁₂₃₄₅₆₇₈₉₀", s);
	}
}