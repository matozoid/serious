package com.laamella.serious;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

class Y {
	public X x;
}

class X {
	public List<Y> y = new ArrayList<>();
	public Map<String, String> items = new HashMap<String, String>();
}

public class ObjectTreeWalkerTest {

	@Test
	public void superScript() {
		System.out.println("");
	}

	@Test
	public void happyFlow() {
		X x = new X();
		Y y = new Y();
		x.y.add(y);
		x.y.add(y);
		x.y.add(y);
		x.y.add(y);
		x.y.add(y);
		x.y.add(new Y());
		y.x = x;
		x.items.put("hello", "world");
		x.items.put("bye", "world");

		ObjectTreeWalker walker = new ObjectTreeWalker();
		Instructions instructions = new DefaultInstructions();
		String serialized = walker.print(x, instructions);
		assertEquals("X₀(items={hello→world,bye→world},y=[Y₁(x=↩X₀),↩Y₁,↩Y₁,↩Y₁,↩Y₁,Y(x=null)])", serialized);
	}
}

